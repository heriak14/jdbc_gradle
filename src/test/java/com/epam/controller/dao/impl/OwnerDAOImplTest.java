package com.epam.controller.dao.impl;

import com.epam.controller.dao.OwnerDAO;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

class OwnerDAOImplTest {

    private static OwnerDAO ownerDAO;

    @BeforeAll
    static void init() {
        ownerDAO = new OwnerDAOImpl();
    }

    @Test
    @Disabled
    void deleteWithSale() {
        try {
            Assert.assertSame(1, ownerDAO.deleteWithSale(1, 2));
        } catch (IllegalAccessException | SQLException | InstantiationException e) {
            Assert.fail(e.getMessage());
        }
    }
}