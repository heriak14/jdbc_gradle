package com.epam.view.impl.entity;

import com.epam.controller.dao.CityDAO;
import com.epam.controller.dao.impl.CityDAOImpl;
import com.epam.model.tables.City;
import com.epam.view.Menu;
import com.epam.view.Printable;
import com.epam.view.impl.EntityView;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

public class CityView extends EntityView {
    private CityDAO cityDAO;

    public CityView() {
        cityDAO = new CityDAOImpl();
    }

    @Override
    public Map<String, Printable> setMethods() {
        Map<String, Printable> userMethods = new LinkedHashMap<>();
        userMethods.put("1", this::findAll);
        userMethods.put("2", this::findByKey);
        userMethods.put("3", this::create);
        userMethods.put("4", this::update);
        userMethods.put("5", this::delete);
        userMethods.put("6", this::showMostVisitedCity);
        userMethods.put("Q", this::quit);
        return userMethods;
    }

    @Override
    protected Map<String, String> setMenu() {
        Map<String, String> menu = new Menu("properties/crud_menu").getMenu();
        menu.put("6", "Show most visited city");
        return menu;
    }

    @Override
    protected void findAll() {
        try {
            cityDAO.findAll().forEach(c -> LOGGER.info(c + "\n"));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void findByKey() {
        Integer id = getIntInput();
        try {
            LOGGER.info(cityDAO.findByKey(id));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void create() {
        City city = inputCity();
        try {
            cityDAO.create(city);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void update() {
        City city = inputCity();
        try {
            cityDAO.update(city);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private City inputCity() {
        City city = new City();
        Integer id = getIntInput();
        city.setId(id);
        LOGGER.info("Enter city name: ");
        city.setName(SCANNER.nextLine());
        LOGGER.info("Enter country: ");
        city.setCountry(SCANNER.nextLine());
        LOGGER.info("Enter description: ");
        city.setDescription(SCANNER.nextLine());
        return city;
    }

    @Override
    protected void delete() {
        try {
            Integer id = getIntInput();
            LOGGER.info(cityDAO.delete(id));
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private Integer getIntInput() {
        LOGGER.info("Enter id: ");
        String id = SCANNER.nextLine();
        if (!id.matches("\\d+")) {
            LOGGER.error("Wrong id! Must be integer.");
            return 0;
        }
        return Integer.parseInt(id);
    }

    private void showMostVisitedCity() {
        try {
            LOGGER.info(cityDAO.findMostVisitedCity());
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.info(e.getMessage());
        }
    }
}
