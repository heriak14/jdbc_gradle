package com.epam.controller.dao;

import com.epam.model.tables.City;

import java.sql.SQLException;

public interface CityDAO extends AbstractDAO<Integer, City> {
    City findMostVisitedCity() throws SQLException, InstantiationException, IllegalAccessException;
}
