package com.epam.controller.dao.impl;

import com.epam.controller.dao.AbstractDAO;
import com.epam.controller.transformer.Transformer;
import com.epam.model.connection.DBConnector;
import com.epam.model.tables.PlanedTrip;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

public class PlanedTripDAO implements AbstractDAO<Integer, PlanedTrip> {
    private static final String FIND_ALL_QUERY = "SELECT * FROM planed_trip";
    private static final String FIND_BY_KEY_QUERY = "SELECT * FROM planed_trip WHERE id = (?)";
    private static final String CREATE_QUERY = "INSERT INTO planed_trip VALUES (?, ?, ?, ?, ?)";
    private static final String UPDATE_QUERY = "UPDATE planed_trip SET id = ?, name = ?, user_id = ?, city_id = ?, date = ? WHERE id = ?";
    private static final String DELETE_QUERY = "DELETE FROM planned_trip WHERE id = ?";
    private Transformer<PlanedTrip> transformer;

    public PlanedTripDAO() {
        transformer = new Transformer<>(PlanedTrip.class);
    }

    @Override
    public Set<PlanedTrip> findAll() throws SQLException, InstantiationException, IllegalAccessException {
        try (Statement statement = DBConnector.getConnection().createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL_QUERY);
            Set<PlanedTrip> planedTrips = new HashSet<>();
            while (resultSet.next()) {
                planedTrips.add((PlanedTrip) transformer.transform(resultSet));
            }
            return planedTrips;
        }
    }

    @Override
    public PlanedTrip findByKey(Integer key) throws SQLException, InstantiationException, IllegalAccessException {
        try (PreparedStatement statement = DBConnector.getConnection().prepareStatement(FIND_BY_KEY_QUERY)) {
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return (PlanedTrip) transformer.transform(resultSet);
            } else {
                return new PlanedTrip();
            }
        }
    }

    @Override
    public int create(PlanedTrip entity) throws SQLException {
        try (PreparedStatement preparedStatement = prepareStatement(CREATE_QUERY, entity)) {
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(PlanedTrip entity) throws SQLException {
        try (PreparedStatement preparedStatement = prepareStatement(UPDATE_QUERY, entity)) {
            preparedStatement.setString(6, String.valueOf(entity.getId()));
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer key) throws SQLException {
        try (PreparedStatement preparedStatement = DBConnector.getConnection().prepareStatement(DELETE_QUERY)) {
            preparedStatement.setString(1, String.valueOf(key));
            return preparedStatement.executeUpdate();
        }
    }

    private PreparedStatement prepareStatement(String query, PlanedTrip entity) throws SQLException {
        try (PreparedStatement preparedStatement = DBConnector.getConnection().prepareStatement(query)) {
            preparedStatement.setString(1, String.valueOf(entity.getId()));
            preparedStatement.setString(2, entity.getName());
            preparedStatement.setString(3, String.valueOf(entity.getUserID()));
            preparedStatement.setString(4, String.valueOf(entity.getCityID()));
            preparedStatement.setString(5, String.valueOf(entity.getDate()));
            return preparedStatement;
        }
    }
}
