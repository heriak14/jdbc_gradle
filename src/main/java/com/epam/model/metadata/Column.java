package com.epam.model.metadata;

import java.util.Objects;

public class Column {
    private String name;
    private String type;
    private String size;
    private boolean nullable;
    private boolean autoincrement;
    private boolean primaryKey;
    private boolean foreignKey;
    private String defaultValue;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    public void setAutoincrement(boolean autoincrement) {
        this.autoincrement = autoincrement;
    }

    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    public void setForeignKey(boolean foreignKey) {
        this.foreignKey = foreignKey;
    }

    boolean isPrimaryKey() {
        return primaryKey;
    }

    boolean isForeignKey() {
        return foreignKey;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public String toString() {
        return String.format("\t%-30s %s %-10s %s %s %s\n", name,
                type, "(" + size + ")",
                (nullable ? "NULL" : "NOT NULL"),
                (autoincrement ? "AUTOINCREMENT" : ""),
                (Objects.nonNull(defaultValue) ? (" DEFAULT " + defaultValue) : ""));
    }
}
