package com.epam.model.tables;

import com.epam.model.annotations.Column;
import com.epam.model.annotations.PrimaryKey;
import com.epam.model.annotations.Table;

import java.sql.Date;
import java.util.Objects;

@Table(name = "feedback")
public class Feedback {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "user_id")
    private Integer userID;
    @Column(name = "city_establishment_id")
    private Integer cityEstablishmentID;
    @Column(name = "feedback")
    private String feedback;
    @Column(name = "date")
    private Date date;
    @Column(name = "rating")
    private Double rating;

    public Feedback() {
    }

    public Feedback(Integer id, Integer userID, Integer cityEstablishmentID, String feedback, Date date, Double rating) {
        this.id = id;
        this.userID = userID;
        this.cityEstablishmentID = cityEstablishmentID;
        this.feedback = feedback;
        this.date = date;
        this.rating = rating;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public Integer getCityEstablishmentID() {
        return cityEstablishmentID;
    }

    public void setCityEstablishmentID(int cityEstablishmentID) {
        this.cityEstablishmentID = cityEstablishmentID;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Feedback feedback1 = (Feedback) o;
        return id.equals(feedback1.id) &&
                userID.equals(feedback1.userID) &&
                cityEstablishmentID.equals(feedback1.cityEstablishmentID) &&
                Double.compare(feedback1.rating, rating) == 0 &&
                feedback.equals(feedback1.feedback) &&
                date.equals(feedback1.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userID, cityEstablishmentID, feedback, date, rating);
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "id=" + id +
                ", userID=" + userID +
                ", cityEstablishmentID=" + cityEstablishmentID +
                ", feedback='" + feedback + '\'' +
                ", date='" + date + '\'' +
                ", rating=" + rating +
                '}';
    }
}
