package com.epam.view.impl;

import com.epam.controller.metadata.DBMetaData;
import com.epam.view.Menu;
import com.epam.view.Printable;
import com.epam.view.View;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

public class MainView implements View {
    private Map<String, String> mainMenu;
    private Map<String, Printable> mainMethods;

    public MainView() {
        mainMenu = new Menu("properties/main_menu").getMenu();
        mainMethods = setMethods();
    }

    @Override
    public Map<String, Printable> setMethods() {
        Map<String, Printable> mainMethods = new LinkedHashMap<>();
        mainMethods.put("1", this::showDBStructure);
        mainMethods.put("2", this::showTables);
        mainMethods.put("Q", this::quit);
        return mainMethods;
    }

    private void showTables() {
        new TableView().show();
    }

    private void showDBStructure() {
        try {
            DBMetaData metaData = new DBMetaData();
            LOGGER.info(metaData.getMetaData());
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private void quit() {
    }

    @Override
    public void printMenu() {
        LOGGER.info("_______________________"
                + "MAIN MENU______________________\n");
        for (Map.Entry entry : mainMenu.entrySet()) {
            LOGGER.info(entry.getKey() + " - " + entry.getValue() + "\n");
        }
        LOGGER.info("____________________________________________\n");
    }

    @Override
    public void show() {
        String option;
        do {
            printMenu();
            LOGGER.info("Choose one option: ");
            option = SCANNER.nextLine().toLowerCase().trim();
            if(mainMethods.containsKey(option)){
                mainMethods.get(option).print();
            }else {
                LOGGER.info("Wrong input! Try again.");
            }
        } while (!option.equals("q"));
    }
}
