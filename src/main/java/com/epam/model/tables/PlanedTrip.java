package com.epam.model.tables;

import com.epam.model.annotations.Column;
import com.epam.model.annotations.PrimaryKey;
import com.epam.model.annotations.Table;

import java.sql.Date;
import java.util.Objects;

@Table(name = "planed_trip")
public class PlanedTrip {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "user_id")
    private Integer userID;
    @Column(name = "city_id")
    private Integer cityID;
    @Column(name = "date")
    private Date date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getCityID() {
        return cityID;
    }

    public void setCityID(int cityID) {
        this.cityID = cityID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlanedTrip that = (PlanedTrip) o;
        return id.equals(that.id) &&
                userID.equals(that.userID) &&
                cityID.equals(that.cityID) &&
                name.equals(that.name) &&
                date.equals(that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, userID, cityID, date);
    }

    @Override
    public String toString() {
        return "PlanedTrip{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", userID=" + userID +
                ", cityID=" + cityID +
                ", date='" + date + '\'' +
                '}';
    }
}
