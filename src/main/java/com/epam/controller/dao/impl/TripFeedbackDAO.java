package com.epam.controller.dao.impl;

import com.epam.controller.dao.AbstractDAO;
import com.epam.controller.transformer.Transformer;
import com.epam.model.connection.DBConnector;
import com.epam.model.tables.TripFeedback;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

public class TripFeedbackDAO implements AbstractDAO<Integer, TripFeedback> {
    private static final String FIND_ALL_QUERY = "SELECT * FROM trip_feedback;";
    private static final String FIND_BY_KEY_QUERY = "SELECT * FROM trip_feedback WHERE id = (?)";
    private static final String CREATE_QUERY = "INSERT INTO trip_feedback VALUES (?, ?, ?)";
    private static final String UPDATE_QUERY = "UPDATE trip_feedback SET id = ?, planed_trip_id = ?, feedback_id = ? WHERE id = ?";
    private static final String DELETE_QUERY = "DELETE FROM trip_feedback WHERE id = ?";
    private Transformer<TripFeedback> transformer;

    public TripFeedbackDAO() {
        transformer = new Transformer<>(TripFeedback.class);
    }

    @Override
    public Set<TripFeedback> findAll() throws SQLException, InstantiationException, IllegalAccessException {
        try (Statement statement = DBConnector.getConnection().createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL_QUERY);
            Set<TripFeedback> tripFeedback = new HashSet<>();
            while (resultSet.next()) {
                tripFeedback.add((TripFeedback) transformer.transform(resultSet));
            }
            return tripFeedback;
        }
    }

    @Override
    public TripFeedback findByKey(Integer key) throws SQLException, InstantiationException, IllegalAccessException {
        try (PreparedStatement statement = DBConnector.getConnection().prepareStatement(FIND_BY_KEY_QUERY)) {
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return (TripFeedback) transformer.transform(resultSet);
            } else {
                return new TripFeedback();
            }
        }
    }

    @Override
    public int create(TripFeedback entity) throws SQLException {
        return prepareStatement(CREATE_QUERY, entity).executeUpdate();
    }

    @Override
    public int update(TripFeedback entity) throws SQLException {
        try (PreparedStatement preparedStatement = prepareStatement(UPDATE_QUERY, entity)) {
            preparedStatement.setString(4, String.valueOf(entity.getId()));
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer key) throws SQLException {
        try (PreparedStatement preparedStatement = DBConnector.getConnection().prepareStatement(DELETE_QUERY)) {
            preparedStatement.setString(1, String.valueOf(key));
            return preparedStatement.executeUpdate();
        }
    }

    private PreparedStatement prepareStatement(String query, TripFeedback entity) throws SQLException {
        try (PreparedStatement preparedStatement = DBConnector.getConnection().prepareStatement(query)) {
            preparedStatement.setString(1, String.valueOf(entity.getId()));
            preparedStatement.setString(2, String.valueOf(entity.getPlanedTripID()));
            preparedStatement.setString(3, String.valueOf(entity.getFeedbackID()));
            return preparedStatement;
        }
    }
}
