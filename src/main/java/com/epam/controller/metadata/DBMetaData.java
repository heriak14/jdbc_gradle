package com.epam.controller.metadata;

import com.epam.model.connection.DBConnector;
import com.epam.model.metadata.Column;
import com.epam.model.metadata.Table;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DBMetaData {
    private DatabaseMetaData metaData;

    public DBMetaData() throws SQLException {
        metaData = DBConnector.getConnection().getMetaData();
    }

    public String getMetaData() throws SQLException {
        return getTablesInfo().stream()
                .map(Table::toString)
                .collect(Collectors.joining());
    }

    private List<Table> getTablesInfo() throws SQLException {
        ResultSet resultSet = metaData.getTables(DBConnector.getConnection().getCatalog(), "trip_advisor",
                "trip_advisor.%", new String[]{"TABLE"});
        List<Table> tables = new ArrayList<>();
        while (resultSet.next()) {
            String name = resultSet.getString("TABLE_NAME");
            Table table = new Table();
            table.setName(name);
            table.setColumns(getColumnsInfo(name));
            tables.add(table);
        }
        return tables;
    }

    private List<Column> getColumnsInfo(String tableName) throws SQLException {
        ResultSet resultSet = metaData.getColumns(DBConnector.getConnection().getCatalog(), null, tableName, null);
        List<Column> columns = new ArrayList<>();
        while (resultSet.next()) {
            Column column = new Column();
            column.setName(resultSet.getString("COLUMN_NAME"));
            column.setType(resultSet.getString("TYPE_NAME"));
            column.setSize(resultSet.getString("COLUMN_SIZE"));
            column.setNullable(resultSet.getString("IS_NULLABLE").equals("YES"));
            column.setAutoincrement(resultSet.getString("IS_AUTOINCREMENT").equals("YES"));
            column.setPrimaryKey(getPrimaryKeyNames(tableName).contains(column.getName()));
            column.setForeignKey(getForeignKeyNames(tableName).contains(column.getName()));
            column.setDefaultValue(resultSet.getString("COLUMN_DEF"));
            columns.add(column);
        }
        return columns;
    }

    private List<String> getPrimaryKeyNames(String tableName) throws SQLException {
        ResultSet resultSet = metaData.getPrimaryKeys(DBConnector.getConnection().getCatalog(), null, tableName);
        List<String> keys = new ArrayList<>();
        while (resultSet.next()) {
            keys.add(resultSet.getString("COLUMN_NAME"));
        }
        return keys;
    }

    private List<String> getForeignKeyNames(String tableName) throws SQLException {
        ResultSet resultSet = metaData.getImportedKeys(DBConnector.getConnection().getCatalog(), null, tableName);
        List<String> keys = new ArrayList<>();
        while (resultSet.next()) {
            keys.add(resultSet.getString("FKCOLUMN_NAME"));
        }
        return keys;
    }

    public static void main(String[] args) throws SQLException {
        DBMetaData data = new DBMetaData();
        System.out.println(data.getMetaData());
    }
}
