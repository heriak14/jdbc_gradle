package com.epam.view.impl;

import com.epam.view.Menu;
import com.epam.view.Printable;
import com.epam.view.View;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class EntityView implements View {
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;

    public EntityView() {
        menu = setMenu();
        menuMethods = setMethods();
    }

    @Override
    public Map<String, Printable> setMethods() {
        Map<String, Printable> userMethods = new LinkedHashMap<>();
        userMethods.put("1", this::findAll);
        userMethods.put("2", this::findByKey);
        userMethods.put("3", this::create);
        userMethods.put("4", this::update);
        userMethods.put("5", this::delete);
        userMethods.put("Q", this::quit);
        return userMethods;
    }

    protected abstract void findAll();

    protected abstract void findByKey();

    protected abstract void create();

    protected abstract void update();

    protected abstract void delete();

    protected Map<String, String> setMenu() {
        return new Menu("properties/crud_menu").getMenu();
    }

    protected void quit() {
    }

    @Override
    public void printMenu() {
        LOGGER.info("_______________________"
                + "TABLE MENU______________________\n");
        for (Map.Entry<String, String> entry : menu.entrySet()) {
            LOGGER.info(entry.getKey() + " - " + entry.getValue() + "\n");
        }
        LOGGER.info("____________________________________________\n");
    }

    @Override
    public void show() {
        String option;
        do {
            printMenu();
            LOGGER.info("Choose one option: ");
            option = SCANNER.nextLine().toLowerCase().trim();
            if (menuMethods.containsKey(option)) {
                menuMethods.get(option).print();
            } else {
                LOGGER.info("Wrong input! Try again.");
            }
        } while (!option.equals("q"));
    }
}
