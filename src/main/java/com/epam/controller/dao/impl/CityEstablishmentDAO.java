package com.epam.controller.dao.impl;

import com.epam.controller.dao.AbstractDAO;
import com.epam.controller.transformer.Transformer;
import com.epam.model.connection.DBConnector;
import com.epam.model.tables.CityEstablishment;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

public class CityEstablishmentDAO implements AbstractDAO<Integer, CityEstablishment> {
    private static final String FIND_ALL_QUERY = "SELECT * FROM city_establishment;";
    private static final String FIND_BY_KEY_QUERY = "SELECT * FROM city_establishment WHERE id = (?)";
    private static final String CREATE_STATEMENT = "INSERT INTO city_establishment VALUES (?, ?, ?, ?, ?)";
    private static final String UPDATE_QUERY = "UPDATE city_establishment SET id = ?, city_id = ?, establishment_id = ?, "
            + "street = ?, building = ? WHERE id = ?";
    private static final String DELETE_QUERY = "DELETE FROM city_establishment WHERE id = ?";
    private Transformer<CityEstablishment> transformer;

    public CityEstablishmentDAO() {
        transformer = new Transformer<>(CityEstablishment.class);
    }

    @Override
    public Set<CityEstablishment> findAll() throws SQLException, InstantiationException, IllegalAccessException {
        try (Statement statement = DBConnector.getConnection().createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL_QUERY);
            Set<CityEstablishment> cityEstablishments = new HashSet<>();
            while (resultSet.next()) {
                cityEstablishments.add((CityEstablishment) transformer.transform(resultSet));
            }
            return cityEstablishments;
        }
    }

    @Override
    public CityEstablishment findByKey(Integer key) throws IllegalAccessException, SQLException, InstantiationException {
        try (PreparedStatement statement = DBConnector.getConnection().prepareStatement(FIND_BY_KEY_QUERY)) {
            statement.setString(1, String.valueOf(key));
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return (CityEstablishment) transformer.transform(resultSet);
            } else {
                return new CityEstablishment();
            }
        }
    }

    @Override
    public int create(CityEstablishment entity) throws SQLException {
        try (PreparedStatement preparedStatement = prepareStatement(CREATE_STATEMENT, entity)) {
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(CityEstablishment entity) throws SQLException {
        try (PreparedStatement preparedStatement = prepareStatement(UPDATE_QUERY, entity)) {
            preparedStatement.setString(6, String.valueOf(entity.getId()));
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer key) throws SQLException {
        try(PreparedStatement preparedStatement = DBConnector.getConnection().prepareStatement(DELETE_QUERY)) {
            preparedStatement.setString(1, String.valueOf(key));
            return preparedStatement.executeUpdate();
        }
    }

    private PreparedStatement prepareStatement(String query, CityEstablishment entity) throws SQLException {
        PreparedStatement preparedStatement = DBConnector.getConnection().prepareStatement(query);
        preparedStatement.setString(1, String.valueOf(entity.getId()));
        preparedStatement.setString(2, String.valueOf(entity.getCityID()));
        preparedStatement.setString(3, String.valueOf(entity.getEstablishmentID()));
        preparedStatement.setString(4, entity.getStreet());
        preparedStatement.setString(5, entity.getBuilding());
        return preparedStatement;
    }
}
