package com.epam.model.tables;

import com.epam.model.annotations.Column;
import com.epam.model.annotations.PrimaryKey;
import com.epam.model.annotations.Table;

import java.util.Objects;

@Table(name = "trip_feedback")
public class TripFeedback {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "planed_trip_id")
    private Integer planedTripID;
    @Column(name = "feedback_id")
    private Integer feedbackID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlanedTripID() {
        return planedTripID;
    }

    public void setPlanedTripID(int planedTripID) {
        this.planedTripID = planedTripID;
    }

    public int getFeedbackID() {
        return feedbackID;
    }

    public void setFeedbackID(int feedbackID) {
        this.feedbackID = feedbackID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TripFeedback that = (TripFeedback) o;
        return id.equals(that.id) &&
                planedTripID.equals(that.planedTripID) &&
                feedbackID.equals(that.feedbackID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, planedTripID, feedbackID);
    }

    @Override
    public String toString() {
        return "TripFeedback{" +
                "id=" + id +
                ", plannedTripID=" + planedTripID +
                ", feedbackID=" + feedbackID +
                '}';
    }
}
