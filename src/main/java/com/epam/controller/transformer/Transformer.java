package com.epam.controller.transformer;

import com.epam.model.annotations.Column;
import com.epam.model.annotations.Table;

import java.lang.reflect.Field;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transformer<T> {
    private Class<T> clazz;

    public Transformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Object transform(ResultSet resultSet) throws SQLException, IllegalAccessException, InstantiationException {
        if (!isSuitableTable(resultSet.getMetaData().getTableName(1))) {
            return null;
        }
        Object row = clazz.newInstance();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(Column.class)) {
                Column column = field.getAnnotation(Column.class);
                if (field.getType() == Integer.class) {
                    field.set(row, resultSet.getInt(column.name()));
                } else if (field.getType() == String.class) {
                    field.set(row, resultSet.getString(column.name()));
                } else if (field.getType() == Date.class) {
                    field.set(row, resultSet.getDate(column.name()));
                } else if (field.getType() == Double.class) {
                    field.set(row, resultSet.getDouble(column.name()));
                } else if (field.getType() == char[].class) {
                    field.set(row, resultSet.getString(column.name()).toCharArray());
                }
            }
        }
        return row;
    }

    private boolean isSuitableTable(String name) {
        if (!clazz.isAnnotationPresent(Table.class)){
            return false;
        }
        String tableName = clazz.getAnnotation(Table.class).name();
        return tableName.equals(name);
    }
}
