package com.epam.model.metadata;

import java.util.List;
import java.util.stream.Collectors;

public class Table {
    private String name;
    private List<Column> columns;

    public void setName(String name) {
        this.name = name;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    @Override
    public String toString() {
        return "TABLE " + name + " (\n"
                + columns.stream().map(Column::toString).collect(Collectors.joining())
                + "\tPRIMARY KEY (" + columns.stream().filter(Column::isPrimaryKey).map(Column::getName)
                .collect(Collectors.joining(", ")) + ")\n"
                + "\tFOREIGN KEY (" + columns.stream().filter(Column::isForeignKey).map(Column::getName)
                .collect(Collectors.joining(", ")) + ")\n"
                + "),\n";
    }
}
