package com.epam.model.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;

public class DBConnector implements Serializable, Cloneable {
    private static final Logger LOGGER = LogManager.getLogger();
    private final String url = Property.DB.getProperty("url");
    private final String user = Property.DB.getProperty("user");
    private final String password = Property.DB.getProperty("password");
    private static Connection connection;
    private static DBConnector instance;

    private DBConnector() {
        if (Objects.nonNull(instance)) {
            throw new IllegalStateException("DBConnector instance already created!");
        }
    }

    public static Connection getConnection() {
        if (Objects.isNull(instance)) {
            instance = new DBConnector();
        }
        try {
            connection = DriverManager.getConnection(instance.url, instance.user, instance.password);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
        return connection;
    }

    public static void shutdown() {
        if(Objects.isNull(connection)) {
            return;
        }
        try {
            connection.close();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    protected Object readResolve() {
        return instance;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return instance;
    }
}
