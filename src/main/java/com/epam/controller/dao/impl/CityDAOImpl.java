package com.epam.controller.dao.impl;

import com.epam.controller.dao.CityDAO;
import com.epam.controller.transformer.Transformer;
import com.epam.model.connection.DBConnector;
import com.epam.model.tables.City;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

public class CityDAOImpl implements CityDAO {
    private static final String FIND_ALL_QUERY = "SELECT * FROM city;";
    private static final String FIND_BY_KEY_QUERY = "SELECT * FROM city WHERE id = ?";
    private static final String CREATE_QUERY = "INSERT INTO city (id, name, country, description) VALUES (?, ?, ?, ?)";
    private static final String UPDATE_QUERY = "UPDATE city SET id = ?, name = ?, country = ?, description = ? WHERE id = ?";
    private static final String DELETE_QUERY = "DELETE FROM city WHERE id = ?";
    private static final String FIND_SPECIFIC = "SELECT id, name, country, description FROM (SELECT city.id, name, " +
            "country, description, count(*) AS city_count FROM feedback " +
            "JOIN city_establishment ON feedback.city_establishment_id = city_establishment.id JOIN city ON " +
            "city_establishment.city_id = city.id GROUP BY name ORDER BY city_count DESC) AS counter LIMIT 1;";
    private Transformer<City> transformer;

    public CityDAOImpl() {
        transformer = new Transformer<>(City.class);
    }

    @Override
    public Set<City> findAll() throws SQLException, InstantiationException, IllegalAccessException {
        try (Statement statement = DBConnector.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
             ResultSet rs = statement.executeQuery(FIND_ALL_QUERY)) {
            Set<City> cities = new HashSet<>();
            while (rs.next()) {
                cities.add((City) transformer.transform(rs));
            }
            return cities;
        }
    }

    @Override
    public City findByKey(Integer key) throws SQLException, InstantiationException, IllegalAccessException {
        try (PreparedStatement statement = DBConnector.getConnection().prepareStatement(FIND_BY_KEY_QUERY)) {
            statement.setString(1, String.valueOf(key));
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return (City) transformer.transform(rs);
            } else {
                return new City();
            }
        }
    }

    @Override
    public int create(City entity) throws SQLException {
        return prepareStatement(CREATE_QUERY, entity).executeUpdate();
    }

    @Override
    public int update(City city) throws SQLException {
        try (PreparedStatement statement = prepareStatement(UPDATE_QUERY, city)) {
            statement.setString(5, String.valueOf(city.getId()));
            return statement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer key) throws SQLException {
        try (PreparedStatement statement = DBConnector.getConnection().prepareStatement(DELETE_QUERY)) {
            statement.setString(1, String.valueOf(key));
            return statement.executeUpdate();
        }
    }

    private PreparedStatement prepareStatement(String query, City entity) throws SQLException {
        try (PreparedStatement statement = DBConnector.getConnection().prepareStatement(query)) {
            statement.setString(1, String.valueOf(entity.getId()));
            statement.setString(2, entity.getName());
            statement.setString(3, entity.getCountry());
            statement.setString(4, entity.getDescription());
            return statement;
        }
    }

    @Override
    public City findMostVisitedCity() throws SQLException, InstantiationException, IllegalAccessException {
        try (Statement statement = DBConnector.getConnection().createStatement()) {
            ResultSet rs = statement.executeQuery(FIND_SPECIFIC);
            if (rs.next()) {
                return (City) transformer.transform(rs);
            } else {
                return new City();
            }
        }
    }
}
