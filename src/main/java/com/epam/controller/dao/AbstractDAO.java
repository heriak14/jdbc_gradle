package com.epam.controller.dao;

import java.sql.SQLException;
import java.util.Set;

public interface AbstractDAO<K, R> {
    Set<R> findAll() throws SQLException, InstantiationException, IllegalAccessException;

    R findByKey(K key) throws SQLException, InstantiationException, IllegalAccessException;

    int create(R entity) throws SQLException;

    int update(R entity) throws SQLException;

    int delete(K key) throws SQLException;
}
