package com.epam.view.impl.entity;

import com.epam.controller.dao.OwnerDAO;
import com.epam.controller.dao.impl.OwnerDAOImpl;
import com.epam.model.tables.Owner;
import com.epam.view.Menu;
import com.epam.view.Printable;
import com.epam.view.impl.EntityView;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

public class OwnerView extends EntityView {
    private OwnerDAO ownerDAO;

    public OwnerView() {
        ownerDAO = new OwnerDAOImpl();
    }

    @Override
    public Map<String, Printable> setMethods() {
        Map<String, Printable> userMethods = new LinkedHashMap<>();
        userMethods.put("1", this::findAll);
        userMethods.put("2", this::findByKey);
        userMethods.put("3", this::create);
        userMethods.put("4", this::update);
        userMethods.put("5", this::delete);
        userMethods.put("6", this::deleteOwnerWithSale);
        userMethods.put("Q", this::quit);
        return userMethods;
    }

    @Override
    protected Map<String, String> setMenu() {
        Map<String, String> menu = new Menu("properties/crud_menu").getMenu();
        menu.put("6", "Delete owner with establishment sale");
        return menu;
    }

    @Override
    protected void findAll() {
        try {
            ownerDAO.findAll().forEach(o -> LOGGER.info(o + "\n"));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void findByKey() {
        Integer id = getIntInput("owner id");
        try {
            LOGGER.info(ownerDAO.findByKey(id));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void create() {
        Owner owner = inputOwner();
        try {
            ownerDAO.create(owner);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void update() {
        Owner owner = inputOwner();
        try {
            ownerDAO.update(owner);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private Owner inputOwner() {
        Owner owner = new Owner();
        Integer id = getIntInput("id");
        owner.setId(id);
        LOGGER.info("Enter city name: ");
        owner.setName(SCANNER.nextLine());
        LOGGER.info("Enter country: ");
        owner.setCountry(SCANNER.nextLine());
        LOGGER.info("Enter city: ");
        owner.setCity(SCANNER.nextLine());
        LOGGER.info("Enter email: ");
        owner.setEmail(SCANNER.nextLine());
        return owner;
    }

    @Override
    protected void delete() {
        try {
            Integer id = getIntInput("id");
            LOGGER.info(ownerDAO.delete(id));
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private Integer getIntInput(String name) {
        LOGGER.info("Enter " + name + ": ");
        String id = SCANNER.nextLine();
        if (!id.matches("\\d+")) {
            LOGGER.error("Wrong id! Must be integer.");
            return 0;
        }
        return Integer.parseInt(id);
    }

    private void deleteOwnerWithSale() {
        Integer sellerId = getIntInput("seller id");
        Integer consumerId = getIntInput("consumer id");
        try {
            ownerDAO.deleteWithSale(sellerId, consumerId);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.info(e.getMessage());
        }
    }
}
