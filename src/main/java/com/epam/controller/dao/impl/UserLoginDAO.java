package com.epam.controller.dao.impl;

import com.epam.controller.dao.AbstractDAO;
import com.epam.controller.transformer.Transformer;
import com.epam.model.connection.DBConnector;
import com.epam.model.tables.UserLogin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

public class UserLoginDAO implements AbstractDAO<Integer, UserLogin> {
    private static final String FIND_ALL_QUERY = "SELECT * FROM user_login;";
    private static final String FIND_BY_KEY_QUERY = "SELECT * FROM user_login WHERE id = (?)";
    private static final String CREATE_QUERY = "INSERT INTO user_login VALUES (?, ?, ?)";
    private static final String UPDATE_QUERY = "UPDATE user_login SET user_id = ?, login = ?, password = ? WHERE id = ?";
    private static final String DELETE_QUERY = "DELETE FROM user_login WHERE id = ?";
    private Transformer<UserLogin> transformer;

    public UserLoginDAO() {
        transformer = new Transformer<>(UserLogin.class);
    }

    @Override
    public Set<UserLogin> findAll() throws SQLException, InstantiationException, IllegalAccessException {
        try (Statement statement = DBConnector.getConnection().createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL_QUERY);
            Set<UserLogin> userLogin = new HashSet<>();
            while (resultSet.next()) {
                userLogin.add((UserLogin) transformer.transform(resultSet));
            }
            return userLogin;
        }
    }

    @Override
    public UserLogin findByKey(Integer key) throws SQLException, InstantiationException, IllegalAccessException {
        try (PreparedStatement statement = DBConnector.getConnection().prepareStatement(FIND_BY_KEY_QUERY)) {
            ResultSet resultSet = statement.executeQuery(FIND_BY_KEY_QUERY);
            if (resultSet.next()) {
                return (UserLogin) transformer.transform(resultSet);
            } else {
                return new UserLogin();
            }
        }
    }

    @Override
    public int create(UserLogin entity) throws SQLException {
        return prepareStatement(CREATE_QUERY, entity).executeUpdate();
    }

    @Override
    public int update(UserLogin entity) throws SQLException {
        try (PreparedStatement preparedStatement = prepareStatement(UPDATE_QUERY, entity)) {
            preparedStatement.setString(4, String.valueOf(entity.getUserID()));
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer key) throws SQLException {
        try (PreparedStatement preparedStatement = DBConnector.getConnection().prepareStatement(DELETE_QUERY)) {
            preparedStatement.setString(1, String.valueOf(key));
            return preparedStatement.executeUpdate();
        }
    }

    private PreparedStatement prepareStatement(String query, UserLogin entity) throws SQLException {
        try (PreparedStatement preparedStatement = DBConnector.getConnection().prepareStatement(query)) {
            preparedStatement.setString(1, String.valueOf(entity.getUserID()));
            preparedStatement.setString(2, entity.getLogin());
            preparedStatement.setString(3, String.valueOf(entity.getPassword()));
            return preparedStatement;
        }
    }
}
