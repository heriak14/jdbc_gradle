package com.epam.view.impl.entity;

import com.epam.controller.dao.impl.EstablishmentDAO;
import com.epam.model.tables.Establishment;
import com.epam.view.impl.EntityView;

import java.sql.Date;
import java.sql.SQLException;

public class EstablishmentView extends EntityView {
    private EstablishmentDAO establishmentDAO;

    public EstablishmentView() {
        establishmentDAO = new EstablishmentDAO();
    }

    @Override
    protected void findAll() {
        try {
            establishmentDAO.findAll().forEach(e -> LOGGER.info(e + "\n"));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void findByKey() {
        Integer id = getIntInput("owner id");
        try {
            LOGGER.info(establishmentDAO.findByKey(id));
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void create() {
        Establishment establishment = inputEstablishment();
        try {
            establishmentDAO.create(establishment);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    protected void update() {
        Establishment establishment = inputEstablishment();
        try {
            establishmentDAO.update(establishment);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private Establishment inputEstablishment() {
        Establishment establishment = new Establishment();
        Integer id = getIntInput("id");
        establishment.setId(id);
        LOGGER.info("Enter name: ");
        establishment.setName(SCANNER.nextLine());
        LOGGER.info("Enter type: ");
        establishment.setType(SCANNER.nextLine());
        Integer ownerId = getIntInput("owner id");
        establishment.setOwnerID(ownerId);
        LOGGER.info("Enter establishment year: ");
        establishment.setFoundationYear(Date.valueOf(SCANNER.nextLine()));
        return establishment;
    }

    @Override
    protected void delete() {
        try {
            Integer id = getIntInput("id");
            LOGGER.info(establishmentDAO.delete(id));
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private Integer getIntInput(String name) {
        LOGGER.info("Enter " + name + ": ");
        String id = SCANNER.nextLine();
        if (!id.matches("\\d+")) {
            LOGGER.error("Wrong input! Must be integer.");
            return 0;
        }
        return Integer.parseInt(id);
    }
}
