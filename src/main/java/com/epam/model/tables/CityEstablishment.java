package com.epam.model.tables;

import com.epam.model.annotations.Column;
import com.epam.model.annotations.PrimaryKey;
import com.epam.model.annotations.Table;

import java.util.Objects;

@Table(name = "city_establishment")
public class CityEstablishment {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "city_id")
    private Integer cityID;
    @Column(name = "establishment_id")
    private Integer establishmentID;
    @Column(name = "street")
    private String street;
    @Column(name = "building")
    private String building;

    public CityEstablishment() {
    }

    public CityEstablishment(Integer id, Integer cityID, Integer establishmentID, String street, String building) {
        this.id = id;
        this.cityID = cityID;
        this.establishmentID = establishmentID;
        this.street = street;
        this.building = building;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getCityID() {
        return cityID;
    }

    public void setCityID(int cityID) {
        this.cityID = cityID;
    }

    public Integer getEstablishmentID() {
        return establishmentID;
    }

    public void setEstablishmentID(int establishmentID) {
        this.establishmentID = establishmentID;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CityEstablishment that = (CityEstablishment) o;
        return id.equals(that.id) &&
                cityID.equals(that.cityID) &&
                establishmentID.equals(that.establishmentID) &&
                street.equals(that.street) &&
                building.equals(that.building);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cityID, establishmentID, street, building);
    }

    @Override
    public String toString() {
        return "CityEstablishment{" +
                "id=" + id +
                ", cityID=" + cityID +
                ", establishmentID=" + establishmentID +
                ", street='" + street + '\'' +
                ", building='" + building + '\'' +
                '}';
    }
}
