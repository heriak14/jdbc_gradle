package com.epam.controller.dao.impl;

import com.epam.controller.dao.OwnerDAO;
import com.epam.controller.transformer.Transformer;
import com.epam.model.connection.DBConnector;
import com.epam.model.tables.Establishment;
import com.epam.model.tables.Owner;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class OwnerDAOImpl implements OwnerDAO {
    private static final String FIND_ALL_QUERY = "SELECT * FROM owner";
    private static final String FIND_BY_KEY_QUERY = "SELECT * FROM owner WHERE id = (?)";
    private static final String CREATE_QUERY = "INSERT INTO owner VALUES (?, ?, ?, ?, ?)";
    private static final String UPDATE_QUERY = "UPDATE owner SET id = ?, name = ?, country = ?, city = ?, email = ? WHERE id = ?";
    private static final String DELETE_QUERY = "DELETE FROM owner WHERE id = ?";
    private Transformer<Owner> transformer;

    public OwnerDAOImpl() {
        transformer = new Transformer<>(Owner.class);
    }

    @Override
    public Set<Owner> findAll() throws SQLException, InstantiationException, IllegalAccessException {
        try (Statement statement = DBConnector.getConnection().createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL_QUERY);
            Set<Owner> owners = new HashSet<>();
            while (resultSet.next()) {
                owners.add((Owner) transformer.transform(resultSet));
            }
            return owners;
        }
    }

    @Override
    public Owner findByKey(Integer key) throws SQLException, InstantiationException, IllegalAccessException {
        try (PreparedStatement statement = DBConnector.getConnection().prepareStatement(FIND_BY_KEY_QUERY)) {
            statement.setString(1, String.valueOf(key));
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return (Owner) transformer.transform(resultSet);
            } else {
                return new Owner();
            }
        }
    }

    @Override
    public int create(Owner entity) throws SQLException {
        try (PreparedStatement preparedStatement = prepareStatement(CREATE_QUERY, entity)) {
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Owner entity) throws SQLException {
        try (PreparedStatement preparedStatement = prepareStatement(UPDATE_QUERY, entity)) {
            preparedStatement.setString(6, String.valueOf(entity.getId()));
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer key) throws SQLException {
        try (PreparedStatement preparedStatement = DBConnector.getConnection().prepareStatement(DELETE_QUERY)) {
            preparedStatement.setString(1, String.valueOf(key));
            return preparedStatement.executeUpdate();
        }
    }

    private PreparedStatement prepareStatement(String query, Owner entity) throws SQLException {
        try (PreparedStatement preparedStatement = DBConnector.getConnection().prepareStatement(query)) {
            preparedStatement.setString(1, String.valueOf(entity.getId()));
            preparedStatement.setString(2, entity.getName());
            preparedStatement.setString(3, entity.getCountry());
            preparedStatement.setString(4, entity.getCity());
            preparedStatement.setString(5, entity.getEmail());
            return preparedStatement;
        }
    }

    @Override
    public int deleteWithSale(int sellerId, int customerId) throws SQLException, IllegalAccessException, InstantiationException {
        if (Objects.isNull(findByKey(customerId)) || Objects.isNull(findByKey(sellerId))) {
            return 0;
        }
        int doneOperations = 0;
        try {
            Set<Establishment> establishments = new EstablishmentDAO().findAll().stream()
                    .filter(e -> e.getOwnerID().equals(sellerId))
                    .collect(Collectors.toSet());
            DBConnector.getConnection().setAutoCommit(false);
            sellEstablishments(establishments, customerId);
            doneOperations = delete(sellerId);
            DBConnector.getConnection().commit();
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            DBConnector.getConnection().rollback();
        } finally {
            DBConnector.getConnection().setAutoCommit(true);
        }
        return doneOperations;
    }

    private void sellEstablishments(Set<Establishment> establishments, int customerId) throws SQLException {
        for (Establishment est : establishments) {
            est.setOwnerID(customerId);
            new EstablishmentDAO().update(est);
        }
    }
}
