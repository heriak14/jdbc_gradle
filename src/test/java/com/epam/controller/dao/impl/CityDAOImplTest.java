package com.epam.controller.dao.impl;

import com.epam.controller.transformer.Transformer;
import com.epam.model.connection.DBConnector;
import com.epam.model.tables.City;
import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;

class CityDAOImplTest {
    private static Connection connection;
    private static CityDAOImpl cityDAO;
    private static Transformer<City> transformer;

    @BeforeAll
    @Test
    static void initConnection() {
        connection = DBConnector.getConnection();
        cityDAO = new CityDAOImpl();
        transformer = new Transformer<>(City.class);
    }

    @Test
    void findAll() {
        try {
            Set<City> cities = cityDAO.findAll();
            Assert.assertNotNull(cities);
            Assert.assertTrue(cities.size() > 0);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    void findByKey() {
        try {
            City city = cityDAO.findByKey(1);
            Assert.assertNotNull(city);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    void findMostVisitedCity() {
        try {
            City city = cityDAO.findMostVisitedCity();
            Assert.assertNotNull(city);
            Assert.assertEquals("Madrid", city.getName());
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            Assert.fail(e.getMessage());
        }
    }

    @AfterAll
    static void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            Assert.fail(e.getMessage());
        }
    }
}